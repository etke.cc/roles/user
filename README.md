# system/user

That role creates user and group

You can configure the following vars:

* `system_user_name` - username
* `system_user_group` - groupname
* `system_user_home` - user's home dir

The following facts will be available after the role execution:

* `system_user_uid` - user's UID
* `system_user_gid` - user's GID
* `system_user_home` - user's home dir
